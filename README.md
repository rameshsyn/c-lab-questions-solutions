# c-lab-questions-solutions
It is a repo to store my solutions of c programming lab questions. BedICT 1st semester lab questions set.  

---  
Note: These programs may not run on windows environment,
      Make sure to include following header file and codes.  
      
      ```
      /* Include this header file */  
      #include <conio.h>  

      int main() {
        // Clear screen on every program run
        // Include this code after variable declaration
        clrscr();

        // Get charater on screen
        // Include this code just before closing brace } of main function
        getch();
      }
      ```
